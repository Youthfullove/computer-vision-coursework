import tkinter as tk
from tkinter import filedialog, messagebox
from PIL import Image, ImageTk
from panorama import process_video

def process_file():
    filepath = filedialog.askopenfilename(filetypes=[("Video files", "*.mp4;*.avi")])
    if filepath:
        try:
            process_video(filepath)
            status_label.config(text="Processing complete. Final panorama saved.")
        except Exception as e:
            messagebox.showerror("Error", f"An error occurred: {str(e)}")

def show_panorama(image_path):
    img = Image.open(image_path)
    photo = ImageTk.PhotoImage(img)
    canvas.config(width=img.width, height=img.height)
    canvas.create_image(0, 0, anchor=tk.NW, image=photo)
    canvas.image = photo

# Create the main application window
root = tk.Tk()
root.title("Panorama Processing")

# Center the window
window_width = 400
window_height = 300
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
x = (screen_width - window_width) // 2
y = (screen_height - window_height) // 2
root.geometry(f"{window_width}x{window_height}+{x}+{y}")

# Create a frame to hold the widgets
frame = tk.Frame(root, padx=10, pady=10)
frame.pack()

# Create a button to select a video file
select_button = tk.Button(frame, text="Select Video File", command=process_file, relief="raised", borderwidth=2)
select_button.grid(row=0, column=0, pady=10)

# Create a label to display status
status_label = tk.Label(frame, text="")
status_label.grid(row=1, column=0, pady=5)

# Create a canvas to display the panorama image
canvas = tk.Canvas(root)
canvas.pack(pady=10)

# Run the Tkinter event loop
root.mainloop()

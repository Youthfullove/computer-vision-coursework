import cv2 as cv
import numpy as np
from preprocess import preprocess_image

def process_video(filename):
    """Process a video to create a panoramic image."""
    cp = cv.VideoCapture(filename)

    if not cp.isOpened():
        print("The video file cannot be opened. Please check the file path or file format.")
        return

    n_frames = int(cp.get(cv.CAP_PROP_FRAME_COUNT))
    print("Enter the number of frames for the video: {}".format(n_frames))

    width = int(cp.get(cv.CAP_PROP_FRAME_WIDTH))
    height = int(cp.get(cv.CAP_PROP_FRAME_HEIGHT))
    fps = cp.get(cv.CAP_PROP_FPS)
    print("FPS: {}".format(fps))

    success, prev = cp.read()
    if success:
        prev = preprocess_image(prev)  # Preprocessed first frame
        # Create SIFT detector
        sift = cv.SIFT_create()

        count = 0
        stitcher = cv.Stitcher.create(cv.Stitcher_PANORAMA)
        final_pano = None

        try:
            while True:
                print("Processing frame: {}".format(int(cp.get(cv.CAP_PROP_POS_FRAMES))))
                cp.set(cv.CAP_PROP_POS_FRAMES, count)
                succ, curr = cp.read()
                if not succ:
                    break

                curr = preprocess_image(curr)  # Preprocess current frame

                # Detect SIFT feature points and descriptors
                kp1, des1 = sift.detectAndCompute(prev, None)
                kp2, des2 = sift.detectAndCompute(curr, None)

                # Feature point matching using FLANN matcher
                FLANN_INDEX_KDTREE = 1
                index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
                search_params = dict(checks=50)
                flann = cv.FlannBasedMatcher(index_params, search_params)

                matches = flann.knnMatch(des1, des2, k=2)

                # Apply Lowe's ratio test to preserve a good match
                good_matches = []
                for m, n in matches:
                    if m.distance < 0.75 * n.distance:
                        good_matches.append(m)

                # Gets the coordinates of the matching points
                src_pts = np.float32([kp1[m.queryIdx].pt for m in good_matches]).reshape(-1, 1, 2)
                dst_pts = np.float32([kp2[m.trainIdx].pt for m in good_matches]).reshape(-1, 1, 2)

                # The homography matrix was estimated by RANSAC algorithm
                M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC, 5.0)

                # mosaic image
                status, pano = stitcher.stitch([prev, curr])

                if status == cv.Stitcher_OK:
                    final_pano = pano  # Update the final panorama
                    prev = pano

                count += 50  # Reduce frame spacing to improve stitching opportunities and quality

        except Exception as e:
            print("An error occurred during concatenation:", e)

        finally:
            cp.release()  # Turn off the video stream

        if final_pano is not None:
            cv.imwrite("final_pano2.png", final_pano)
            print("Final panorama saved.")
    else:
        print("Failed to read the first frame of the video.")

# Example usage:
# process_video('sample-cut.mp4')

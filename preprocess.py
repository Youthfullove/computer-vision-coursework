import cv2 as cv
import numpy as np


def adjust_gamma(image, gamma=1.0):
    """ Apply gamma correction to adjust image brightness """
    inv_gamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** inv_gamma) * 255 for i in np.arange(0, 256)]).astype("uint8")
    return cv.LUT(image, table)


def adjust_brightness_contrast(image, brightness=0, contrast=0):
    """ Adjust the brightness and contrast of the image """
    if brightness != 0:
        if brightness > 0:
            shadow = brightness
            highlight = 255
        else:
            shadow = 0
            highlight = 255 + brightness
        alpha_b = (highlight - shadow) / 255
        gamma_b = shadow

        buf = cv.addWeighted(image, alpha_b, image, 0, gamma_b)
    else:
        buf = image.copy()

    if contrast != 0:
        f = 131 * (contrast + 127) / (127 * (131 - contrast))
        alpha_c = f
        gamma_c = 127 * (1 - f)

        buf = cv.addWeighted(buf, alpha_c, buf, 0, gamma_c)

    return buf


def preprocess_image(image):
    """ The image is preprocessed to enhance the feature detection effect, while the brightness is adjusted to
    improve the visual effect"""
    # Contrast enhancement using CLAHE
    if len(image.shape) == 3:
        lab = cv.cvtColor(image, cv.COLOR_BGR2LAB)
        l, a, b = cv.split(lab)
        clahe = cv.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        l_clahe = clahe.apply(l)
        lab_clahe = cv.merge([l_clahe, a, b])
        image_clahe = cv.cvtColor(lab_clahe, cv.COLOR_LAB2BGR)
    else:
        clahe = cv.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        image_clahe = clahe.apply(image)

    # Slightly sharpen the image
    kernel = np.array([[-1, -1, -1],
                       [-1, 9, -1],
                       [-1, -1, -1]]) * 0.5  # Reduce sharpening intensity
    image_sharpened = cv.filter2D(image_clahe, -1, kernel)

    # Apply gamma correction and brightness contrast adjustment
    image_gamma_corrected = adjust_gamma(image_sharpened, gamma=1.0)  # Lower the gamma value to brighten the image
    image_bright_contrast_adjusted = adjust_brightness_contrast(image_gamma_corrected, brightness=90,
                                                                contrast=60)  # Increase brightness and contrast

    return image_bright_contrast_adjusted